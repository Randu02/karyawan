package com.example.demo.controller;

import com.example.demo.entity.Karyawan;
import com.example.demo.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class KaryawanController {
    @Autowired
    private KaryawanService service;

    @PostMapping("/addKaryawan")
    public Karyawan addKaryawan(@RequestBody Karyawan karyawan) {
        return service.saveKaryawan(karyawan);
    }

    @PostMapping("/addKaryawans")
    public List<Karyawan> addKaryawans(@RequestBody List<Karyawan> karyawans) {
        return service.saveKaryawans(karyawans);
    }

    @GetMapping("/karyawans")
    public List<Karyawan> findAllKaryawans() {
        return service.getKaryawans();
    }

    @GetMapping("/karyawan/{Nik}")
    public Karyawan findKaryawanById(@PathVariable int Nik) {
        return service.getKaryawanById(Nik);
    }

    @GetMapping("/karyawanByName/{nama}")
    public Karyawan findKaryawanByName(@PathVariable String name) {

        return service.getKaryawanByName(name);

    }

    @PutMapping("/update")
    public  Karyawan updateKaryawan(@RequestBody  Karyawan  karyawan){
        return service.saveKaryawan(karyawan);
    }
    @DeleteMapping("/delete/{Nik}")
    public String deleteProduct(@PathVariable int Nik) {
        return service.deleteKaryawan(Nik);
    }
}
