package com.example.demo.service;

import com.example.demo.entity.Karyawan;
import com.example.demo.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class KaryawanService {
    @Autowired
    private KaryawanRepository repository;

    public Karyawan saveKaryawan(Karyawan karyawan){
        return repository.save(karyawan);
    }
    public List<Karyawan> saveKaryawans(List<Karyawan> karyawans){
        return repository.saveAll(karyawans);
    }
    public List<Karyawan> getKaryawans(){
        return repository.findAll();
    }
    public Karyawan getKaryawanById(int Nik){
        return repository.findById(Nik).orElse(null);
    }
    public Karyawan getKaryawanByName(String name){
        return repository.findByName(name);
    }
    public String deleteKaryawan(int Nik) {
        repository.deleteById(Nik);
        return "Product removed!!";
    }

//    public Karyawan updateKaryawan(Karyawan  karyawan) {
//        Karyawan existingKaryawan = repository.findById(karyawan.getId()).orElse(null);
//        existingKaryawan.setNik(karyawan.getNik());
//        existingKaryawan.setName(karyawan.getName());
//        existingKaryawan.setTempat(karyawan.getTempat());
//        existingKaryawan.setAlamat(karyawan.getAlamat());
//        existingKaryawan.setAgama(karyawan.getAgama());
//        existingKaryawan.setStatus_pernikahan(karyawan.getStatus_pernikahan());
//        existingKaryawan.setPekerjaan(karyawan.getPekerjaan());
//        existingKaryawan.setKewarganegaraan(karyawan.getKewarganegaraan());
//        existingKaryawan.setGaji(karyawan.getGaji());
//        existingKaryawan.setJk(karyawan.getJk());
//
//        return repository.save(existingKaryawan);
//    }
}
