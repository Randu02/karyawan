package com.example.demo.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "data_karyawan")
public class Karyawan {
    @Id
    private int Nik;
    private String name;
    private String tempat;
    private String alamat;
    private String agama;
    private String status_pernikahan;
    private String pekerjaan;
    private String kewarganegaraan;
    private String Gaji;
    private String jk;
}
