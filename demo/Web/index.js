const baseUrl = "http://localhost:8000/karyawans";
let jsonReq = "";

document.querySelector("#submit").onclick = function () {
    var nik = document.getElementById("nik").value;
    var nama = document.getElementById("nama").value;
    var ttl = document.getElementById("ttl").value;
    var jk = document.getElementById("jk").value;
    var alamat = document.getElementById("alamat").value;
    var agama = document.getElementById("agama").value;
    var stts = document.getElementById("stts").value;
    var gaji = document.getElementById("gaji").value;
    var pekerjaan = document.getElementById("pekerjaan").value;
    var kwr = document.getElementById("kwr").value;
    
    jsonReq = JSON.stringify({
        nik: nik,
        name: nama,
        tempat: ttl,
        jk: jk,
        alamat: alamat,
        agama: agama,
        status_pernikahan: stts,
        pekerjaan: pekerjaan,
        gaji: gaji,
        kewarganegaraan: kwr,
    })
     postData(jsonReq);
};

function postData(body) {
    fetch(`http://localhost:8000/addKaryawan`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    })
        .then(res => {
            console.log(res)
            return res.json()
        })
        .then(data => console.log(data))
        .catch(console.error);
}