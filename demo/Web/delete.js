const baseUrl = "http://localhost:8000/karyawans";
let jsonDel = "";

document.querySelector("#delete").onclick = function() {            
    const nik = document.getElementById("nik").value;
    deleteData(nik);
};

function deleteData(nik) {
    console.log(nik);
    fetch(`http://localhost:8000/delete/${nik}`, {
        method: 'DELETE',
    })
    .then(res => { res.json() 
        console.log(res)
     })
    .catch(console.error);
}