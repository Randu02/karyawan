const baseUrl = "http://localhost:8000/karyawans";
let jsonReq = "";


const getData = () => {
    fetch(`${baseUrl}`)
    .then((response) => {
          return response.json();
    })
    .then((responseJson) => {
          renderData(responseJson);

    })
    .catch((error) => {
          console.log(error);
    });
}

const renderData = results => {
  const tbody = document.querySelector("#tbody");
  tbody.innerHTML = "";
  
  results.forEach( res => {
       tbody.innerHTML += `
        <tr>
              
              <td>${res.nik}</td>
              <td>${res.name}</td>
              <td>${res.tempat}</td>
              <td>${res.jk}</td>
              <td>${res.alamat}</td>
              
                <td><!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${res.name}Detail">
                 Detail
                </button>
                
                <!-- Modal -->
                <div class="modal fade" id="${res.name}Detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                       <form>
                       <div class="form-group">
                       <label style="font-weight : bold">Nik</label>
                       <br>
                       ${res.nik} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Nama</label>
                       <br>
                       ${res.name} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Tempat tanggal lahir</label>
                       <br>
                       ${res.tempat} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Jenis Kelamin</label>
                       <br>
                       ${res.jk} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Alamat</label>
                       <br>
                       ${res.alamat} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Agama</label>
                       <br>
                       ${res.agama} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Status pernikahan</label>
                       <br>
                       ${res.status_pernikahan} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Pekerjaan</label>
                       <br>
                       ${res.pekerjaan} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Gaji</label>
                       <br>
                       ${res.gaji} 
                       </div>
                       <div class="form-group">
                       <label style="font-weight : bold">Kewarganegaraan</label>
                       <br>
                       ${res.kewarganegaraan} 
                       </div>
                       </form>
                      </div>
                      <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                       
                      </div>
                    </div>
                  </div>
                </div>
               <!-- Button trigger modal -->
               <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${res.name}update">
                Update
               </button>
               
               <!-- Modal -->
               <div class="modal fade" id="${res.name}update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog">
                   <div class="modal-content">
                     <div class="modal-header">
                       <h5 class="modal-title" id="exampleModalLabel">Update</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                       </button>
                     </div>
                     <div class="modal-body">
        
        
                     <input type="hidden" class="form-control" id="edit-nik"  value="${res.nik}">
                    
        
                   <div class="form-group">
                     <label>Nama</label>
                     <input type="text" class="form-control" id="edit-nama" value="${res.name}">
                   </div>
                   <div class="form-group">
                       <label>Tempat, Tanggal lahir </label>
                       <input type="text" class="form-control" id="edit-ttl" value="${res.tempat}">
                     </div>
                     <label>Jenis Kelamin </label>
                     <select class="form-control" id="jk">
                       <option>${res.jk}</option>
                       <option>Laki-laki</option>
                       <option>Perempuan</option>
                     </select>
                     <div class="form-group">
                       <label>Alamat </label>
                       <input type="text" class="form-control" id="edit-alamat" value="${res.alamat}">
                     </div>
                     <div class="form-group">
                       <label>Agama </label>
                       <input type="text" class="form-control" id="edit-agama" value="${res.agama}">
                     </div>
                     <label>Status perkawinan </label>
                     <select class="form-control" id="stts">
                       <option>${res.status_pernikahan}</option>
                       <option>BELUM KAWIN</option>
                       <option>KAWIN</option>
                     </select>
                     <div class="form-group">
                       <label>Pekerjaan</label>
                       <input type="text" class="form-control" id="edit-pekerjaan" value="${res.pekerjaan}">
                     </div>
                     <div class="form-group">
                       <label>Gaji</label>
                       <input type="text" class="form-control" id="edit-gaji" value="${res.gaji}">
                     </div>
                     <div class="form-group">
                       <label>kewarganegaran</label>
                       <input type="text" class="form-control" id="edit-kwr" value="${res.kewarganegaraan}">
                     </div>
              
                     </div>
                     <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                       <button type='button' class='btn btn-primary' onClick='updateEach(`+res.nik +`)' >Save changes</button>
                     </div>
                   </div>
                 </div>
               </div></td>

        </tr>`;
  });
  
}

function updateEach(nik){
 
    var nik = document.getElementById("edit-nik").value;
    var nama = document.getElementById("edit-nama").value;
    var ttl = document.getElementById("edit-ttl").value;
    var jk = document.getElementById("jk").value;
    var alamat = document.getElementById("edit-alamat").value;
    var agama = document.getElementById("edit-agama").value;
    var stts = document.getElementById("stts").value;
    var gaji = document.getElementById("edit-gaji").value;
    var pekerjaan = document.getElementById("edit-pekerjaan").value;
    var kwr = document.getElementById("edit-kwr").value;
    
    jsonReq = JSON.stringify({
        nik: nik,
        name: nama,
        tempat: ttl,
        jk: jk,
        alamat: alamat,
        agama: agama,
        status_pernikahan: stts,
        pekerjaan: pekerjaan,
        gaji: gaji,
        kewarganegaraan: kwr
    })
     

    fetch("http://localhost:8000/update", {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body : jsonReq
    })
        .then(res => {
            console.log(res)
            return res.json()
           
        })
        .then(data => console.log(data))
         alert("berhasil")
        location.reload()
        .catch(console.error);
      
}

getData();
